package DataAccess;

import Model.*;
import org.bouncycastle.crypto.agreement.jpake.JPAKERound1Payload;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Used for connecting to order table</h1>
 */
public class OrderDAO extends AbstractDAOTwo<Order>{

    /**
     * <h1>Insert a new order</h1>
     * @param order to be inserted
     */
    public void insertOrder(Order order){
        super.insert(order);
    }

    /**
     * <h1>Generate detailed orders</h1>
     * @param content list of orders based on which the detailed orders are generated
     * @return list of detailed orders
     */
    public List<DetailedOrder> getDetailedOrders(List<Order> content){

        Client client = null;
        Product product = null;
        Receipt recipe = null;
        ClientDAO clientDAO = new ClientDAO();//access to client table
        ProductDAO productDAO = new ProductDAO();//access to product table
        ReceiptDAO recipeDAO = new ReceiptDAO();//access to recipe table

        List<DetailedOrder> list = new ArrayList<DetailedOrder>();

        try {

            for (Order order : content) {

                client = clientDAO.findById(order.getClientId());//find client based on clientID
                product = productDAO.findById(order.getProductId());//find product based on productID
                recipe = recipeDAO.findById(order.getReceiptId());//find recipe based on recipeID

                list.add(new DetailedOrder(order.getId(), client.getName(), product.getName(), recipe.getQuantity(), recipe.getTotal(), product.getAvailable()));
            }

            return list;

        }catch(NullPointerException e){

            e.printStackTrace();
        }

        return null;
    }

    /**
     * <h1>Update entry</h1>
     * @param id ID of the entry to be updated
     * @param field field based on which the update is performed
     * @param newValue the new value to be set
     */
    public void updateField(int id, String field, Object newValue){

        super.update(id, field, newValue);
    }

    /**
     * <h1>Delete entry</h1>
     * @param field field based on which the deletion is performed
     * @param value value to be checked
     */
    public void deleteOrder(String field, Object value){

        super.delete(field, value, false);
    }

    /**
     * <h1>Get ID of entry based on a single condition</h1>
     * @param field field based on which selection is performed
     * @param value the value to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    public int getOrderID(String field, Object value){

        return super.getID(field, value);
    }
}

