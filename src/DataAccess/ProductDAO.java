package DataAccess;

import Model.*;
public class ProductDAO extends AbstractDAOTwo<Product>{

    /**
     * <h1>Insert a new product</h1>
     * @param product to be inserted
     */
    public void insertProduct(Product product){

        super.insert(product);
    }

    /**
     * <h1>Used to update product based on field</h1>
     * @param id ID of the entry to be updated
     * @param field field based on which the update is performed
     * @param newValue the new value to be set
     */
    public void updateField(int id, String field, Object newValue){

        super.update(id, field, newValue);
    }

    /**
     * <h1>Delete entry</h1>
     * @param field field based on which the deletion is performed
     * @param value value to be checked
     */
    public void deleteProduct(String field, Object value){

        Product product = super.findById(getProductID(field, value));

        if(product != null)//if found
            updateNewValues(product, new Product(product.getID(), product.getName(), 0, product.getPrice(), false));//update quantity and status
    }

    /**
     * <h1>Get ID of entry based on a single condition</h1>
     * @param field field based on which selection is performed
     * @param value the value to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    public int getProductID(String field, Object value){

        return super.getID(field, value);
    }

    /**
     * <h1>Update values of old product based on new product</h1>
     * @param oldProduct old product
     * @param newProduct new product
     */
    public void updateNewValues(Product oldProduct, Product newProduct){

        if(newProduct.getQuantity() != oldProduct.getQuantity())
            this.updateField(oldProduct.getID(), "quantity", newProduct.getQuantity());

        if(newProduct.getPrice() != oldProduct.getPrice())
            this.updateField(oldProduct.getID(), "price", newProduct.getPrice());

        if(newProduct.getAvailable() != oldProduct.getAvailable())
            this.updateField(oldProduct.getID(), "available", newProduct.getAvailable());
    }
}
