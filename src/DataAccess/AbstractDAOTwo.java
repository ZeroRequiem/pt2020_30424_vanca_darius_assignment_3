package DataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class AbstractDAOTwo<T> extends AbstractDAO<T>{

    /**
     * <h1>Find entry based on id</h1>
     * @param id the id of the entry
     * @return object of entry if successful, null otherwise
     */
    public T findById(int id){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String query = createSelectQuery("id");

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if(!resultSet.isBeforeFirst())//result set is empty, entry couldn't be found
                return null;

            return createObjects(resultSet).get(0);

        }catch (SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByID " + e.getMessage());
        }finally { ConnectionFactory.closeAll(resultSet, statement, connection); }

        return null;
    }

    /**
     * <h1>Used for getting ID of entry based on one or more fields</h1>
     * @param query the query to be executed
     * @param values the values to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    private int getQuery(String query, Object[] values){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            if(values != null)
                setStatement(statement, values);

            resultSet = statement.executeQuery();

            if(resultSet.next())//if not empty
                return resultSet.getInt(1);
            else
                return -1;//not found

        }catch (Exception e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:get ID " + e.getMessage());
        }finally {
            ConnectionFactory.closeAll(resultSet, statement, connection);
        }

        return -1;
    }

    /**
     * <h1>Select all entries</h1>
     * @return list of objects found in the specified table if successful, null otherwise
     */
    public List<T> selectAll(){ return selectAll(null, null); }

    /**
     * <h1>Get ID of entry based on a single condition</h1>
     * @param field field based on which selection is performed
     * @param value the value to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    public int getID(String field, Object value){

        Object[] values = {value};

        return this.getQuery("select id from `" + type.getSimpleName() + "` where " + field + " = ?", values);
    }

    /**
     * <h1>Get ID of entry based on 2 conditions</h1>
     * @param field1 first field based on which selection is performed
     * @param field2 second field based on which selection is performed
     * @param value1 first value to be checked
     * @param value2 second value to be checked
     * @return  ID of entry if successful, -1 otherwise
     */
    public int getID(String field1, String field2, Object value1, Object value2){

        Object[] values = {value1, value2};

        return this.getQuery("select id from `" + type.getSimpleName() + "` where " + field1 + " = ? and " + field2 + " = ?", values);
    }

    /**
     * <h1>Get max id</h1>
     * @return max id if successful, -1 otherwise
     */
    public int getMaxId(){ return this.getQuery("select max(id) from `" + type.getSimpleName() + "`", null); }
}
