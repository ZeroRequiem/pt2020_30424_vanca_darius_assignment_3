package DataAccess;
import Model.*;
/**
 * <h1>Used for connecting to the client table</h1>
 */
public class ClientDAO extends AbstractDAOTwo<Client>{

    /**
     * <h1>Insert a new client</h1>
     * @param client to be inserted
     */
    public void insertClient(Client client){

        super.insert(client);
    }

    /**
     * <h1>Update entry</h1>
     * @param id ID of the entry to be updated
     * @param field field based on which the update is performed
     * @param newValue the new value to be set
     */
    public void updateField(int id, String field, Object newValue){

        super.update(id, field, newValue);
    }

    /**
     * <h1>Delete entry</h1>
     * @param field field based on which the deletion is performed
     * @param value value to be checked
     */
    public void deleteClient(String field, Object value){

        super.delete(field, value, false);
    }

    /**
     * <h1>Get ID of entry based on 2 conditions</h1>
     * @param field1 first field based on which selection is performed
     * @param field2 second field based on which selection is performed
     * @param value1 first value to be checked
     * @param value2 second value to be checked
     * @return  ID of entry if successful, -1 otherwise
     */
    public int getClientID(String field1, String field2, Object value1, Object value2){

        return super.getID(field1, field2, value1, value2);
    }

    /**
     * <h1>Get ID of entry based on a single condition</h1>
     * @param field field based on which selection is performed
     * @param value the value to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    public int getClientID(String field, Object value){

        return super.getID(field, value);
    }

    public void updateNewValues(Client oldClient, Client newClient){

        if(!newClient.getAddress().equals(oldClient.getAddress()))
            this.updateField(oldClient.getID(), "address", newClient.getAddress());
    }
}
