package DataAccess;
import Model.*;
import java.util.List;

/**
 * <h1>Used for connecting to the recipe table</h1>
 */
public class ReceiptDAO extends AbstractDAOTwo<Receipt>{

    /**
     * <h1>Delete all recipe</h1>
     * @param orders
     */
    public void deleteAll(List<Order> orders){

        if(orders != null){
            for(Order order : orders)
                super.delete("id", order.getReceiptId(), false);
        }
    }
}
