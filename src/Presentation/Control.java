package Presentation;

import BLL.*;
import Model.*;
import DataAccess.*;
import Presentation.*;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <h1>Class used to interact with the user commands and data base</h1>
 */
public class Control {

    private final MyFileParser fileParser;//used for parsing the command file

    //used for generating the pdf files
    private PDFgenerator<Client> clientReport;
    private PDFgenerator<Product> productReport;
    private PDFgenerator<DetailedOrder> orderReport;

    //used for generating the id of the pdf files
    private int reportID;

    private BLL businessLogicLayer;

    /**
     * @param path path of file containing the commands
     */
    public Control(String path){

        fileParser = new MyFileParser(path);
        businessLogicLayer = new BLL();

        clientReport = new PDFgenerator<Client>();
        productReport = new PDFgenerator<Product>();
        orderReport = new PDFgenerator<DetailedOrder>();

        reportID = 0;

        String command;

        while(fileParser.hasLinesLeft()) {//if lines left continue

            fileParser.setLine();//read the current line

            if((command = fileParser.getCommand()) != null) {//check if any valid command found, if not ignore line

                if(command.contains("Report"))
                    generateReports(command);
                else {

                    businessLogicLayer.performCommand(command, fileParser.getValues());//if found perform command
                    checkStatus(businessLogicLayer.getStatus());

                }
            }
        }

        fileParser.close();
    }

    /**
     * <h1>Used for checking the status of the performed command</h1>
     * @param status the status
     */
    private void checkStatus(String status){

        if(!status.equals("allGood")){

            if(status.equals("Bill"))
                orderReport.generateBill(reportID++, businessLogicLayer.getBill());
            else
                orderReport.generateErrorTicket(status);
        }
    }

    /**
     * <h1>Used for generating the reports</h1>
     * @param command the command
     */
    private void generateReports(String command){

        switch(command){

            case "Report client":

                clientReport.generateReport("clientReport" + reportID++, businessLogicLayer.getAllClients());
                break;

            case "Report product":

                float[] columnWidths = {1.5f, 2f, 2f, 2f, 2f};//used for generating the pdf
                productReport.generateReport("productReport" + reportID++, businessLogicLayer.getAllProducts(), columnWidths);
                break;

            case "Report order":

                float[] columnWidths3 = {1.5f, 1.5f, 1.5f, 1.5f, 1.5f, 1.5f};
                orderReport.generateReport("ordersReport" + reportID++, businessLogicLayer.getDetailedOrders(), columnWidths3);
                break;

            default:
                break;
        }
    }

}
