package Presentation;

import Model.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.beans.PropertyDescriptor;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <h1>Class used for pdf files generation</h1>
 * <p>The concept of reflection was used for some methods in order to not write the same kind o methods over and over</p>
 * @param <T> the generic type
 */
public class PDFgenerator<T> {

    private Font normalFont;
    private Font boldFont;

    @SuppressWarnings("unchecked")
    public PDFgenerator(){

        boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
        normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);
    }

    /**
     * <h1>Create a cell of a table</h1>
     * @param table the table
     * @param text the text to be set
     * @param alignValue align value
     * @param columnSpan column span
     * @param font font of text
     */
    private void createCell(PdfPTable table, String text, int alignValue, int columnSpan, Font font){

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(alignValue);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(columnSpan);
        //in case there is no text and you wan to create an empty row
        if(text.trim().equalsIgnoreCase("")){
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    /**
     * <h1>Generate header cells</h1>
     * @param content content of header cells
     * @param table the table where the cells wil be set
     */
    private void generateHeaderCells(List<T> content, PdfPTable table){

        table.setWidthPercentage(90f);

        for(Field field : content.get(0).getClass().getDeclaredFields())
            createCell(table, field.getName(), Element.ALIGN_RIGHT, 1, boldFont);

        table.setHeaderRows(1);
    }

    /**
     * <h1>Generate a pdf table</h1>
     * @param columnsWidth width of columns
     * @param content list of content to be set
     * @return a pdf table if successful, null otherwise
     */
    private PdfPTable createTable(float[] columnsWidth, List<T> content){

        PdfPTable table = new PdfPTable(columnsWidth);
        Object value = null;

        generateHeaderCells(content, table);

        try {

            for (T current : content) {//for each element

                for (Field field : current.getClass().getDeclaredFields()) {//for each field of current entry

                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), current.getClass());
                    Method method = propertyDescriptor.getReadMethod();
                    value = method.invoke(current);//get the values from each field

                    if(value instanceof String)//align to left if string
                        createCell(table, value.toString(), Element.ALIGN_LEFT, 1, normalFont);
                    else//numeric, date or boolean, align to right
                        createCell(table, value.toString(), Element.ALIGN_RIGHT, 1, normalFont);
                }
            }

            return table;

        }catch (Exception e){ e.printStackTrace(); }

        return null;
    }

    /**
     * <h1>Generates a report</h1>
     * @param docName name of the document
     * @param content list of entries which will make out the
     * @param columnWidths width of columns
     */
    public void generateReport(String docName, List<T> content, float[] columnWidths){

        if(content.size() == 0){//no entries

            generateErrorTicket("REPORT: Table is empty cannot generate " + docName);
            return;
        }

        Document document = new Document();
        PdfWriter pdfWriter = null;

        try {

            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(docName + ".pdf"));

            document.open();

            Paragraph paragraph = new Paragraph("Report " + content.get(0).getClass().getSimpleName());
            document.add(createTable(columnWidths, content));//create a table and add it
            document.add(paragraph);

            document.close();
            pdfWriter.close();

        }catch (Exception e){

            e.printStackTrace();
        }
    }

    /**
     * <h1>Generate a report</h1>
     * @param docName name of document
     * @param content list of entries
     */
    public void generateReport(String docName, List<T> content){

        float[] columnWidths = {1.5f, 2f, 2f};

        this.generateReport(docName, content, columnWidths);

    }

    /**
     * <h1>Generate paragraphs for bill</h1>
     * @param client client entry
     * @param product product entry
     * @param receipt receipt entry
     * @param orderId orderId
     * @return array of paragraphs
     */
    private Paragraph[] generateParagraphs(Client client, Product product, Receipt receipt, int orderId){

        Paragraph[] paragraph = new Paragraph[7];

        paragraph[0] = new Paragraph("Time of transaction: " + receipt.getDate());
        paragraph[1] = new Paragraph("RecipeID:" + receipt.getId());
        paragraph[2] = new Paragraph("OrderID: " + orderId);
        paragraph[3] = new Paragraph("Client: " + client.getName());
        paragraph[4] = new Paragraph("Product: " + product.getName());
        paragraph[5] = new Paragraph("Quantity: " + receipt.getQuantity());
        paragraph[6] = new Paragraph("Total: " + receipt.getTotal());

        return paragraph;
    }

    /**
     * <h1>Generate a bill</h1>
     * @param billNumber the number of bill
     * @param bill the bill
     */
    public void generateBill(int billNumber, Bill bill){

        Document document = new Document();

        try{

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("Bill" + billNumber + ".pdf"));
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(0 ,0, 0));
            document.open();

            Paragraph[] paragraph = generateParagraphs(bill.getClient(), bill.getProduct(), bill.getReceipt(), bill.getOrderId());

            for(int i = 0; i < paragraph.length; i++) {

                paragraph[i].setFont(boldFont);
                document.add(paragraph[i]);
            }

            document.close();
            pdfWriter.close();

        }catch(Exception e){

            e.printStackTrace();
        }
    }

    /**
     * <h1>Used to create an error ticket</h1>
     * @param msg message of the ticket
     */
    public void generateErrorTicket(String msg){

        Document document = new Document();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        try{

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("ErrorTicket" + (int)(Math.random() * 10) + ".pdf"));
            document.open();

            Paragraph paragraph = new Paragraph("Time: " + date);
            Paragraph error = new Paragraph(msg);

            error.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(255 ,0, 0)));

            document.add(paragraph);
            document.add(error);

            document.close();
            pdfWriter.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
