package Presentation;

import javax.sound.midi.Soundbank;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h1>File parser to read commands and get values</h1>
 */
public class MyFileParser {

    private File file;//the file
    private Scanner scan;//used for reading
    private ArrayList<String> commandList;//used for safety reasons
    private String currentLine;//the line which is currently being read

    public MyFileParser(String path){

        file = new File(path);

        try{
            scan = new Scanner(file);
        }catch (FileNotFoundException e){

            System.out.println("No such file");
            System.exit(-1);
        }

        commandList = new ArrayList<String>();

        commandList.add("Insert client");
        commandList.add("Insert product");
        commandList.add("Delete client");
        commandList.add("Delete product");
        commandList.add("Report client");
        commandList.add("Report product");
        commandList.add("Report order");
        commandList.add("Order");

    }

    /**
     * <h1>Set current line</h1>
     */
    public void setLine(){

        currentLine = readContents();
    }

    /**
     * <h1>Get command</h1>
     * @return command in string format if it can be found in the valid command list, null otherwise
     */
    public String getCommand(){

        String[] values = currentLine.split(":");

        if(!commandList.contains(values[0])){

            System.out.println("Invalid command found '" + values[0] + "' ( be careful, commands are case sensitive!!! )");
            return null;
        }

        return values[0];
    }

    /**
     * @return true if file has lines unread, false otherwise
     */
    public boolean hasLinesLeft(){

        return scan.hasNextLine();
    }

    /**
     * @return a line read from the file at current position of cursor
     */
    private String readContents(){

        if(scan.hasNextLine()){

            return scan.nextLine();
        }

        return null;
    }

    /**
     * @return string of values if successful, null otherwise
     */
    public String[] getValues(){

        String[] values = currentLine.split(":");

        if(values.length > 1)
            return getValues(values[1]);
        else
            return null;
    }

    /**
     * <h1>Get values from line</h1>
     * @param line the line
     * @return array of string which holds the values
     */
    private String[] getValues(String line){

        String[] values = line.split(",");

        String[] newValues = new String[values.length];

        for(int i = 0; i < values.length; i++) {

            if(values[i].charAt(0) == ' ')//in case of empty space at first position
                newValues[i] = values[i].substring(1);//delete it
            else
                newValues[i] = values[i];
        }

        return newValues;
    }

    /**
     * close the scanner
     */
    public void close(){

        scan.close();
    }

}
