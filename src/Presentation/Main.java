package Presentation;

import Presentation.Control;

public class Main {

    public static void main(String[] args) {

        if(args.length != 1){

            System.out.println("Invalid number of arguments");
            System.exit(-1);
        }

        Control control = new Control(args[0]);
    }
}
