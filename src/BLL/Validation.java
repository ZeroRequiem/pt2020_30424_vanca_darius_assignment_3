package BLL;
import Model.*;
import java.sql.PreparedStatement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Used for validating various things</h1>
 */
public class Validation {

    private static boolean validate(String pattern, String value){

        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(value);

        if(matcher.matches())
            return  true;

        return false;
    }

    /**
     * @param values to be validated
     * @param expected expected number of values
     * @return true if it respects the expected value, false otherwise
     */
    private static boolean validateValues(String[] values, int expected){

        if(values == null)
            return false;

        return values.length == expected;
    }

    /**
     *-1 because we exclude the ID
     */
    public static boolean validateClientValues(String[] values){

        return validateValues(values, Client.class.getDeclaredFields().length - 1);
    }

    /**
     *-2 because we exclude the product ID and status available
     */
    public static boolean validateProductValues(String[] values){

        return validateValues(values, Product.class.getDeclaredFields().length - 2);
    }

    /**
     * -1 because we exclude order ID
     */
    public static boolean validateOrderValues(String[] values){

        return validateValues(values, Order.class.getDeclaredFields().length - 1);
    }

    public static boolean validateClientName(String value){

        return validate("([A-Z])([a-z]+?)([ ])([A-Z])([a-z]+?)", value);
    }

    public static int validateIntegerNumber(String value){

        try{
            return Integer.parseInt(value);
        }catch (NumberFormatException e){

            return -1;
        }
    }

    public static float validateFloatNumber(String value){

        try{
            return Float.parseFloat(value);
        }catch (NumberFormatException e){

            return -1;
        }
    }
}
