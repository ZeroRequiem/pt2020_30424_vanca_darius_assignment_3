package BLL;

import DataAccess.ClientDAO;
import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import DataAccess.ReceiptDAO;
import Model.*;

import java.sql.Date;
import java.util.List;

/**
 * <h1>Business logic layer</h1>
 */
public class BLL {

    //interactions with the data base are done via the following objects
    private ClientDAO clientAccess;
    private ProductDAO productAccess;
    private OrderDAO orderAccess;
    private ReceiptDAO recipeAccess;
    private Bill bill;
    //used for generating the id of the pdf files
    private int clientID;
    private int productID;
    private int orderID;
    private int recipeID;
    private String status;

    public BLL(){

        clientAccess = new ClientDAO();
        productAccess = new ProductDAO();
        orderAccess = new OrderDAO();
        recipeAccess = new ReceiptDAO();

        clientID = clientAccess.getMaxId() + 1;
        productID = productAccess.getMaxId() + 1;
        orderID = orderAccess.getMaxId() + 1;
        recipeID = recipeAccess.getMaxId() + 1;

        status = "allGood";
    }

    /**
     * <h1>Used for executing the command</h1>
     * @param command the command in string format
     */
    public void performCommand(String command, String[] values){

        status = "allGood";//set status when executing command to 'allGood' and if something happens reset it

        switch(command){

            case "Insert client":

                insertClient(values);
                break;

            case "Insert product":

                insertProduct(values);
                break;

            case "Delete client":

                deleteClient(values);
                break;

            case "Delete product":

                deleteProduct(values);
                break;

            case "Order":

                order(values);
                break;

            default:
                break;
        }
    }

    public List<DetailedOrder> getDetailedOrders(){
        return orderAccess.getDetailedOrders(orderAccess.selectAll());
    }

    public List<Client> getAllClients(){
        return clientAccess.selectAll();
    }

    public List<Product> getAllProducts(){
        return productAccess.selectAll();
    }

    private void setBill(Client client, Product product, Order order, Receipt receipt){

        bill = new Bill(client, product, order.getId(), receipt);
    }

    public Bill getBill(){
        return this.bill;
    }

    public String getStatus(){
        return status;
    }

    /**<h1>Insert the order and update the product and update product</h1>
     * @param values values of command
     * @param product product entry
     * @param client client entry
     */
    private void insertAndUpdate(String[] values, Product product, Client client){

        int quantity = Validation.validateIntegerNumber(values[2]);

        if(quantity <= 0){//validate quantity of order

            status = "ORDER: Invalid format for quantity field :" + values[2] + "'";
            return;
        }

        int currentQuantity = product.getQuantity();//current quantity of product from order
        int newQuantity = currentQuantity - quantity;//new quantity of product after order

        if(newQuantity < 0){//if client is asking for too much

            status = "UNDER-STOCK: client is trying to order an invalid quantity, asked for: " + quantity + " when available is: " + currentQuantity;//generate error ticket
            return;
        }

        Receipt newReceipt = new Receipt(recipeID++, quantity, quantity * product.getPrice(), new Date((new java.util.Date()).getTime()));//generate receipt
        Order newOrder = new Order(orderID++, client.getID(), product.getID(), newReceipt.getId());//generate order

        recipeAccess.insert(newReceipt);//add receipt to data base
        orderAccess.insertOrder(newOrder);//add order to data base
        productAccess.updateField(product.getID(), "quantity", newQuantity);//update quantity field

        if(newQuantity == 0)
            productAccess.updateField(product.getID(), "available", false);//if after the order the product has no more quantity set it to unavailable

        setBill(client, product, newOrder, newReceipt);//generate the bill
        status = "Bill";
    }

    /**<h1>Used for order command</h1>
     * @param values values of command
     */
    private void order(String[] values){

        if(!Validation.validateOrderValues(values)){//validate values

            status = "Not enough values to perform 'Order' command";
            return;
        }

        Client client = clientAccess.findById(clientAccess.getClientID("name", values[0]));

        if(client == null){//if client couldn't be found in data base

            status = "ORDER:Client '" + values[0] + "' could not be found in data base: " + values[0];
            return;
        }

        Product product = productAccess.findById(productAccess.getProductID("name", values[1]));

        if(product == null){//if product couldn't be found in data base

            status = "ORDER:Product could not be found in data base: " + values[1];
            return;
        }

        if(!product.getAvailable())//if product found but is unavailable
            status = "UNDER-STOCK: Product " + product.getName() + " is not available at this moment, order will be ignored";
        else {//everything okey try to generate pdf for bill and update the status of the data base
            insertAndUpdate(values, product, client);
        }
    }

    /**<h1>Delete a product</h1>
     * @param values values of command
     */
    private void deleteProduct(String[] values){

        if(productAccess.getProductID("name", values[0]) < 0)//if product not in data base
            status = "DELETION:Product '" + values[0] + "' couldn't be found in database";
        else
            productAccess.deleteProduct("name", values[0]);
    }

    /**<h1>Delete a client</h1>
     * @param values values of command
     */
    private void deleteClient(String[] values){

        Client client = clientAccess.findById(clientAccess.getClientID("name", values[0]));

        if(client != null) {//if in data base

            Order order = orderAccess.findById(orderAccess.getOrderID("clientId", client.getID()));

            if(order != null){//check if client has any orders placed

                List<Order> ordersToBeDeleted = orderAccess.selectAll("clientId", client.getID());//select all the orders of the client
                orderAccess.deleteOrder("clientId", client.getID());//delete all orders
                recipeAccess.deleteAll(ordersToBeDeleted);//delete receipts of orders
            }

            clientAccess.deleteClient("id", client.getID());//delete client
        }else//client not in data base
            status = "DELETION:Client '" + values[0] + "' couldn't be found in database";
    }

    /**<h1>Insert a product</h1>
     * @param values values of command
     */
    private void insertProduct(String[] values){

        if(!Validation.validateProductValues(values)){

            status = "INSERT PRODUCT:Not enough values specified for product insertion";
            return;
        }

        int pID = productAccess.getProductID("name", values[0]);
        int quantity = Validation.validateIntegerNumber(values[1]);
        float price = Validation.validateFloatNumber(values[2]);

        if(price <= 0 || quantity <= 0){//if not valid
            status = "Invalid format for numerical values in insert product '" + values[0] + "' operation";
            return;
        }

        if(pID < 0)//product not in data base add if for the first time
            productAccess.insertProduct(new Product(productID++, values[0], quantity, price, true));
        else {//product found in database

            Product product = productAccess.findById(pID);

            if(!product.getAvailable())//check status, if unavailable make it available with the new values
                productAccess.updateNewValues(product, new Product(pID, values[0], quantity, price, true));
            else
                status = "INSERT PRODUCT: Product already found in data base '" + values[0] + "'";
        }
    }

    /**<h1>Insert a client</h1>
     * @param values values of command
     */
    private void insertClient(String[] values){

        if(!Validation.validateClientValues(values)){

            status = "INSERT CLIENT: Not enough values specified for client insertion";
            return;
        }

        if(!Validation.validateClientName(values[0])) {

            status = "INSERT CLIENT: Invalid name format:" + values[0];
            return;
        }

        int cID = clientAccess.getClientID("name", values[0]);

        if(cID < 0)//if not found add it for the first time
            clientAccess.insertClient(new Client(clientID++, values[0], values[1]));
        else
            status = "Client already found in data base with same name: '" + values[0] + "'";
    }
}
