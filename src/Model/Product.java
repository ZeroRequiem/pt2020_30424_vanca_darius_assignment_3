package Model;

public class Product {

    private int ID;
    private String name;
    private int quantity;
    private float price;
    private boolean available;//used in case product is no longer available

    public Product(int ID, String name, int quantity, float price, boolean available){

        this.ID = ID;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.available = available;
    }

    public Product(){}

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean getAvailable(){
        return available;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setAvailable(boolean value){
        this.available = value;
    }
}
