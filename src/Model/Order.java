package Model;

public class Order {

    private int Id;
    private int clientId;
    private int productId;
    private int receiptId;

    public Order(int id, int customerId, int productId, int receiptId){

        this.Id = id;
        this.clientId = customerId;
        this.productId = productId;
        this.receiptId = receiptId;
    }

    public Order(){}

    public int getReceiptId() {
        return receiptId;
    }

    public int getClientId() {
        return clientId;
    }

    public int getProductId() {
        return productId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int orderId) {
        this.Id = orderId;
    }

    public void setClientId(int customerId) {
        this.clientId = customerId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setReceiptId(int ReceiptId) {
        this.receiptId = ReceiptId;
    }
}
