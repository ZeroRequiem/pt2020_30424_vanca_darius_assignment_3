package Model;
import Model.*;

public class Bill {

    private Client client;
    private Product product;
    private int orderId;
    private Receipt receipt;

    /**
     * <h1>The bill after a transaction</h1>
     * @param client client
     * @param product product
     * @param orderID the order information for order id
     * @param receipt the receipt for additional information
     */
    public Bill(Client client, Product product, int orderID, Receipt receipt){

        this.client = client;
        this.product = product;
        this.orderId = orderID;
        this.receipt = receipt;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
