package Model;

import java.text.SimpleDateFormat;
import java.sql.Date;

public class Receipt {

    private int id;
    private int quantity;
    private float total;
    private Date date;

    /**
     * @param id id of recipe
     * @param quantity quantity of order
     * @param total total sum paid
     * @param date date of order
     */
    public Receipt(int id, int quantity, float total, Date date){

        this.date = date;
        this.id = id;
        this.quantity = quantity;
        this.total = total;
    }

    public Receipt(){}

    public int getQuantity() {
        return quantity;
    }

    public Date getDate() {
        return date;
    }

    public float getTotal() {
        return total;
    }

    public int getId() {
        return id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
