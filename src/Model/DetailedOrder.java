package Model;

/**
 * <h1>Special object used to displayed detailed information about order</h1>
 */
public class DetailedOrder {

    private int orderID;
    private String clientName;
    private String productName;
    private int quantity;
    private float total;
    private String statusProduct;

    /**
     *
     * @param orderID order id
     * @param clientName client name
     * @param productName product name
     * @param quantity quantity of order
     * @param total total paid for order
     * @param available status of product at the moment
     */
    public DetailedOrder(int orderID, String clientName, String productName, int quantity, float total, boolean available){

        this.orderID = orderID;
        this.clientName = clientName;
        this.productName = productName;
        this.quantity = quantity;
        this.total = total;

        if(available)
            statusProduct = "available";
        else
            statusProduct = "unavailable";
    }

    public int getOrderID() {
        return orderID;
    }

    public String getClientName() {
        return clientName;
    }

    public String getProductName() {
        return productName;
    }

    public String getStatusProduct(){return statusProduct;}

    public float getTotal() {
        return total;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setStatusProduct(String newStatus){statusProduct = newStatus;}

    public void setTotal(float total) {
        this.total = total;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
